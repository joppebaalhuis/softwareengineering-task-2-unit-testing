//
// Created by joppe on 2-10-2018.
//

#ifndef GOOGLETESTEXERCISE_CALCULATOR_H
#define GOOGLETESTEXERCISE_CALCULATOR_H

#include <exception>

class calculator
{
    public:
        int add(int Number1,int Number2);
        double add(double Number1,double Number2);
        int subtract(int Number1, int Number2);
        double subtract(double Number1, double Number2);
        int multiply(int Number1, int Number2);
        double multiply(double Number1, double Number2);
        int divide(int Number1, int Number2);
        double divide(double Number1, double Number2);
        int square(int Number);
        double square(double Number);


};



#endif //GOOGLETESTEXERCISE_CALCULATOR_H
