//
// Created by joppe on 2-10-2018.
//

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "calculator.h"
#include <exception>


using testing::Eq;

namespace {
    calculator calculator;

    TEST(Test1, add)
    {
        EXPECT_EQ(10, calculator.add(2,8));
        EXPECT_EQ(10, calculator.add(2.0,8.0));
    };

    TEST(Test2, subtract)
    {
        EXPECT_EQ(-6, calculator.subtract(2,8));
        EXPECT_EQ(-6, calculator.subtract(2.0,8.0));
    };

    TEST(Test3, multiply)
    {
        EXPECT_EQ(16, calculator.multiply(2,8));
        EXPECT_EQ(16, calculator.multiply(2.0,8.0));
    };

    TEST(Test4, divide)
    {
        EXPECT_EQ(4, calculator.divide(8,2));
        EXPECT_EQ(4, calculator.divide(8.0,2.0));
    };

    TEST(Test5, divideByZero)
    {
        try
        {
            calculator.divide(10,0);
            FAIL();
        }
        catch(std::exception& e)
        {
          EXPECT_EQ(e.what(),std::string("divide by zero"));
        }

        try
        {
            calculator.divide(10.0,0.0);
            FAIL();
        }
        catch(std::exception& e)
        {
            EXPECT_EQ(e.what(),std::string("divide by zero"));
        }
    }

    TEST(Test6, square)
    {
        EXPECT_EQ(5, calculator.square(25));
        EXPECT_EQ(5, calculator.square(25));
    };


}
