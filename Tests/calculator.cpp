//
// Created by joppe on 2-10-2018.
//

#include <cmath>
#include "calculator.h"
#include <exception>

class divideByZero: public std::exception
{
    virtual const char* what() const throw()
    {
        return "divide by zero";
    }
} divideByZero;


int calculator::add(int Number1,int Number2)
{
    return Number1 + Number2;
}

double calculator::add(double Number1, double Number2)
{
    return Number1 + Number2;
}

int calculator::subtract(int Number1, int Number2)
{
    return Number1 - Number2;
}

double calculator::subtract(double Number1, double Number2)
{

    return Number1 - Number2;
}

int calculator::multiply(int Number1, int Number2)
{
    return Number1 * Number2;
}

double calculator::multiply(double Number1, double Number2)
{
    return  Number1 * Number2;
}

int calculator::divide(int Number1, int Number2)
{
    if(Number2 == 0)
    {
        throw divideByZero;
    }else
    {
        return Number1/Number2;

    }
}

double  calculator::divide(double Number1, double Number2)
{
    if(Number2 == 0.0)
    {
        throw divideByZero;
    }else
    {
    return Number1/Number2;
    }
}

int calculator::square(int Number)
{
    return std::sqrt(Number);
}

double calculator::square(double Number)
{
    return std::sqrt(Number);
}